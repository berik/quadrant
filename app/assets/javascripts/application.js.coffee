#= require jquery
#= require jquery_ujs
#= require turbolinks
#= require bootstrap-sprockets
#= require nprogress
#= require nprogress-turbolinks
#= require bootbox
#= require bootstrap-datepicker

(($) ->
  $.fn.datepicker.dates["es"] =
    days: [
      "Domingo"
      "Lunes"
      "Martes"
      "Miércoles"
      "Jueves"
      "Viernes"
      "Sábado"
      "Domingo"
    ]
    daysShort: [
      "Dom"
      "Lun"
      "Mar"
      "Mié"
      "Jue"
      "Vie"
      "Sáb"
      "Dom"
    ]
    daysMin: [
      "Do"
      "Lu"
      "Ma"
      "Mi"
      "Ju"
      "Vi"
      "Sa"
      "Do"
    ]
    months: [
      "Enero"
      "Febrero"
      "Marzo"
      "Abril"
      "Mayo"
      "Junio"
      "Julio"
      "Agosto"
      "Septiembre"
      "Octubre"
      "Noviembre"
      "Diciembre"
    ]
    monthsShort: [
      "Ene"
      "Feb"
      "Mar"
      "Abr"
      "May"
      "Jun"
      "Jul"
      "Ago"
      "Sep"
      "Oct"
      "Nov"
      "Dic"
    ]
    today: "Hoy"

  return
) jQuery
