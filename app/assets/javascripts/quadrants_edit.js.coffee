class @QuadrantsEdit
  _users = `undefined`

  constructor: (payload) ->
    { @_users } = payload
    @loadQuadrant()
    @jQueryEvents()

  loadQuadrant: ->
    $.ajax(
      url: "/quadrants/edit"
      type: "GET"
      contentType: "application/json"
      dataType: "JSON"
    ).done((response) =>
      $.each response, (index, day) =>
        if day isnt null
          $.each day.first, (user, content) =>
            $("#day-#{index}-usr-#{user}-type-1").prop "checked", true
            return
          $.each day.second, (user, content) =>
            $("#day-#{index}-usr-#{user}-type-2").prop "checked", true
            return
          $.each day.third, (user, content) =>
            $("#day-#{index}-usr-#{user}-type-3").prop "checked", true
            return
        return
      console.log @_users
      return
    ).error (data) ->
      console.log data
      return

  jQueryEvents: ->
    $("#quadrant-date").datepicker
      language: "es"
      format: "mm/yyyy"
      viewMode: "months"
      minViewMode: "months"

    $("#save-btn").unbind "click"
    $("#save-btn").click (evt) =>
      @makeQuadrant()
      return

  makeQuadrant: ->
    payload =
      checks: {}
    for day in [1..$("#edit-info").data("days")]
      $.each @_users, (index, user) =>
        console.log $("#day-#{day}-usr-#{user}-type-1").is(":checked")
        payload["checks"]["#{day}"] = {} unless payload["checks"]["#{day}"]?
        unless payload["checks"]["#{day}"]["#{user}"]?
          payload["checks"]["#{day}"]["#{user}"] = {}
        if $("#day-#{day}-usr-#{user}-type-1").is(":checked")
          payload["checks"]["#{day}"]["#{user}"]["first"] = true
        if $("#day-#{day}-usr-#{user}-type-2").is(":checked")
          payload["checks"]["#{day}"]["#{user}"]["second"] = true
        if $("#day-#{day}-usr-#{user}-type-3").is(":checked")
          payload["checks"]["#{day}"]["#{user}"]["third"] = true
        return
    console.log payload
    $.ajax(
      url: "/quadrants/edit"
      type: "PATCH"
      data: payload
    ).done((response) =>
      bootbox.alert "Cambios guardados"
    ).error (data) ->
      bootbox.alert "Error"
      console.log data
      return
    return

  window.QuadrantsEdit = QuadrantsEdit

  $(document).on 'ready page:load', =>
    new window.QuadrantsEdit(window.userPayload)
