module QuadrantsIndexHelper
  def draw_quadrant first
    first = Date.today.beginning_of_month unless first.present?
    last = first.end_of_month
    tmp = first
    output = "<div class=\"well\">"
    output += "<table class=\"table table-condensed table-bordered"
    output += "table-striped warning\"><thead>"
    output += "<tr><th>Turno</th><th>Lun</th><th>Mar</th><th>Mie</th>"
    output += "<th>Jue</th><th>Vie</th><th>Sab</th><th>Dom</th></tr></thead>"
    output += "<tbody>"
    week = 7
    begin
      if week == 7
        week = 0
        if tmp == first
          output += "<tr>#{make_td("normal",["#{get_first_shift}","#{get_second_shift}"],"#{tmp.day}")}"
          (tmp.wday - 1).times do |i|
            output += "<td>&nbsp;</td>"
            week += 1
          end
        else
          output += "</tr><tr>"
          output += "#{make_td("normal",["#{get_first_shift}","#{get_second_shift}"],"#{tmp.day}")}"
        end
      end
      if [5,6].include? week
        output += "#{make_td("special",["#{get_weekend(tmp)}"],"#{tmp.day}")}"
      else
        output += "#{make_td("normal",["#{get_first(tmp)}","#{get_second(tmp)}"],"#{tmp.day}")}"
      end
      week += 1
      tmp += 1.day
    end while tmp <= last
    output += "</tr></body></table></div>"
    output
  end

  def make_td type, content, day
    output = "<td><div class=\"well well-sm\" data-content=\"#{day}\">"
    case type
      when "normal"
        output += "<div class=\"row\"><div class=\"col-xs-12\">"
        output += "<div class=\"bg-info text-center\">"
        output += "#{content[0]}</div></div></div>"
        output += "<div class=\"row\"><div class=\"col-xs-12\">"
        output += "<div class=\"bg-success text-center\">"
        output += "#{content[1]}</div></div></div>"
      when "special"
        output += "<div class=\"row\"><div class=\"col-xs-12\">"
        output += "<div class=\"bg-warning text-center\">"
        output += "#{content[0]}</div></div></div>"
    end
    output += "</div></td>"
    output
  end

  def get_first_shift
    output = Hour.shift_hour(1)
    output = output.blank? ? "" : "#{output.first.shift.name} (#{output.first.name})"
    output
  end

  def get_second_shift
    output = Hour.shift_hour(2)
    output = output.blank? ? "" : "#{output.first.shift.name} (#{output.first.name})"
    output
  end

  def get_weekend date
    emp = QuadrantMonth.qdate(date).shift_hour(3)
    if emp.blank?
      output = "&nbsp;"
    else
      output = ""
      emp.each do |o|
        output += "#{o.employee.name} "
      end
    end
    output
  end

  def get_first date
    emp = QuadrantMonth.qdate(date).shift_hour(1)
    if emp.blank?
      output = "&nbsp;"
    else
      output = ""
      emp.each do |o|
        output += "#{o.employee.name} "
      end
    end
    output
  end

  def get_second date
    emp = QuadrantMonth.qdate(date).shift_hour(2)
    if emp.blank?
      output = "&nbsp;"
    else
      output = ""
      emp.each do |o|
        output += "#{o.employee.name} "
      end
    end
    output
  end
end
