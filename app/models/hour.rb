class Hour < ActiveRecord::Base
  has_one :shift

  scope :shift_hour, -> (shift) { joins(:shift).where(:"shifts.id" => shift) }
end
