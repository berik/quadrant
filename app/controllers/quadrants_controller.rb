class QuadrantsController < ApplicationController
layout "home"
require 'active_support/core_ext'

  def index
    @date = Date.today.beginning_of_month
  end

  def edit
    respond_to do |format|
      @date = Date.today.beginning_of_month
      first = @date
      last = first.end_of_month
      tmp = first
      @jsquadrants = true
      format.html {}
      format.json {
        output = []
        output.push nil
        puts first.end_of_month.day
        for index in 1..first.end_of_month.day do
          output.push({first: {}, second: {}, third:{}})
          emp = QuadrantMonth.qdate(tmp).shift_hour(1)
          output[index][:first] = {}
          emp.each do |e|
            output[index][:first]["#{e.employee.id}"] = true
          end
          emp = QuadrantMonth.qdate(tmp).shift_hour(2)
          output[index][:second] = {}
          emp.each do |e|
            output[index][:second]["#{e.employee.id}"] = true
          end
          emp = QuadrantMonth.qdate(tmp).shift_hour(3)
          output[index][:third] = {}
          emp.each do |e|
            output[index][:third]["#{e.employee.id}"] = true
          end
          tmp = tmp + 1.day
        end
        render json: output
      }
    end
  end

  def update
    date = Date.today.beginning_of_month
    QuadrantMonth.erase_month(date).destroy_all
    params[:checks].each do |day, content|
      target_date = Date.new(date.year,date.month,day.to_i)
      content.each do |user, content2|
        target_user = user.to_i
        content2.each do |field, o|
          case field
            when "first"
              target_shift = 1
            when "second"
              target_shift = 2
            when "thid"
              target_shift = 3
          end
          QuadrantMonth.create(
            employee_id: target_user,
            shift_id: target_shift,
            date: target_date)
        end
      end
    end
    render json: "OK"
  end
end
