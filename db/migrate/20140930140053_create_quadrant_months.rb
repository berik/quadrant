class CreateQuadrantMonths < ActiveRecord::Migration
  def change
    create_table :quadrant_months do |t|
      t.date :date, null: false
      t.integer :employee_id, null: false
      t.integer :shift_id, null: false
      t.timestamps
    end
  end
end
